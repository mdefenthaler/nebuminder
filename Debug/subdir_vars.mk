################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../lnk_msp430g2553.cmd 

ASM_SRCS += \
../RTC.asm \
../RTC_TA.asm 

C_SRCS += \
../main.c 

OBJS += \
./RTC.obj \
./RTC_TA.obj \
./main.obj 

ASM_DEPS += \
./RTC.pp \
./RTC_TA.pp 

C_DEPS += \
./main.pp 

C_DEPS__QUOTED += \
"main.pp" 

OBJS__QUOTED += \
"RTC.obj" \
"RTC_TA.obj" \
"main.obj" 

ASM_DEPS__QUOTED += \
"RTC.pp" \
"RTC_TA.pp" 

ASM_SRCS__QUOTED += \
"../RTC.asm" \
"../RTC_TA.asm" 

C_SRCS__QUOTED += \
"../main.c" 


